<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 2.5 License

Name       : Unqualified
Description: A two-column, fixed-width design.
Version    : 1.0
Released   : 20071220

-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>GENSER Validation site</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<script src="scripts.js"></script>
<link href="default.css" rel="stylesheet" type="text/css" media="screen" />
<?php include "config.php"; ?>
</head>
<body>
<!-- start header -->
<?php include "header.php" ?>
<!-- end header -->
<!-- start page -->
<div id="page">
	<!-- start content -->
<?php
  if (isset($_REQUEST["page"])){
    $page = $_REQUEST["page"];
  } else {
    $page = "start";
  };
  if (file_exists(".pages/".$page.".php")){
    include ".pages/".$page.".php";
  } else {
    include ".pages/notfound.php";
  }
?>
	<!-- end content -->
	<!-- start sidebar -->
 	<?php include "sidebar.php" ?>
	<!-- end sidebar -->
</div>
<!-- end page -->
<?php include "footer.php" ?>
</body>
</html>
