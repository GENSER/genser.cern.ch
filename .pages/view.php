<?php
  $tag = $_REQUEST["tag"];
?>
<div id="content">
  <div class="post">
    <h1 class="title">View record data</h1>
    <div class="entry">
<p>The record has the following attributes:
<center>
  <table style="width:60%">
    <tr><td><strong>Attribute</strong></td><td><strong>Value</strong></td></tr>
    <tr><td>Package</td><td><?php echo $DATA[$tag]["package"]?></td></tr>
    <tr><td>Version</td><td><?php echo $DATA[$tag]["version"]?></td></tr>
    <tr><td>Release</td><td><?php echo $DATA[$tag]["release"]?></td></tr>
    <tr><td>Platform</td><td><?php echo $DATA[$tag]["platform"]?></td></tr>
    <tr><td>Date</td><td><?php echo $DATA[$tag]["date"]?></td></tr>
  </table>
</center>
<br>
Available result files:
<ul>
<?php
$CONTENT = yaml_parse_file($config["datapath"]."/".$tag."/content.yaml");
foreach($CONTENT["files"] as $f){
  echo "<li><a href=\"/data/$tag/$f\">$f</a></li>";
}
?>
</ul>
<br>
Available configuration files:
<ul>
<?php
  foreach($CONTENT["configs"] as $config){
    echo "<li><a href=\"$config\">".basename($config)."</a></li>";
  }
?>
</ul>
</p>     
    </div>
  </div>
</div>

