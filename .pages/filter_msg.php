<?php
$req_gen = (isset($_REQUEST["refs"]) ? $_REQUEST["refs"] : "None");
$req_min = (isset($_REQUEST["min"]) ? intval($_REQUEST["min"]) : 10);
$req_max = (isset($_REQUEST["max"]) ? intval($_REQUEST["max"]) : 30);

if ($req_min <= 0) $req_min = 10;
if ($req_max <= 0) $req_max = 30;
if ($req_min > $req_max) {
  $tmp = $req_max;
  $req_max = $req_min;
  $req_min = $tmp;
}

$indent = "                        ";
$gen_list = array();
$gen_list[] = "<option value disabled".(($req_gen == "None")?" selected":"")."> -- select version -- </option>\n";

foreach($refs as $gen) {
  $sel = ($gen == $req_gen)?" selected":"";
  $gen_list[]  = "<option value=\"".urlencode($gen)."\"$sel>$gen</option>\n";
}
?>

<div id="content" style="width: auto">
  <div class="post" style="background: none">
    <h1 class="title">Reference</h1>
    <div class="entry">
      <p>
        <!-- form action="?" method="GET">
          <center>
            <div style="width: 100%">
              <table>
                <tr>
                  <td>Reference version:
                    <select name="refs">
<?php
foreach ($gen_list as $_ => $gen) {
  echo $indent.$gen;
}
?>
                    </select>
                  </td>
                  <td>Warning level: deviation &plusmn;
                    <input name="min" type="text" maxlength="2" value="<? echo $req_min ?>"> percent
                    ; Error level: deviation &plusmn;
                    <input name="max" type="text" maxlength="2" value="<? echo $req_max ?>"> percent
                  </td>
                  <td><input type=submit value="Use"></td>
                </tr>
              </table>
              <br>
              <input type=hidden name=page value=filter_ref>
              <!- - input type=submit value="Select ..." - ->
            </center>
          </div>
        </form-->
      </p>
      <p>
        <center>
          <hr>
          <script type="text/javascript">
          window.onload = function() {
            var sidebar = document.getElementById('sidebar');
            sidebar.style.display = 'none';
            //new Tablesort(document.getElementById('ref'));
          }
          </script>
          <!-- script src='tablesort.min.js'></script>
          <script src='tablesort.numeric.js'></script -->
          <table id="ref">
            <thead>
<?php
  $indent = "                  ";
  echo $indent."<tr style=\"border-bottom: none\"><th><strong>Process group</strong></th><th><strong>Error message</strong></th>";

  //$req_gen = (isset($_REQUEST["refs"]) ? $_REQUEST["refs"] : "None");
  $ref_data = array();
  $row2 = $indent."<tr style=\"border-top: none; border-bottom: 1px solid black\"><th></th>";

  foreach($refs as $gen) {
    //echo "<th colspan=\"2\" class='no-sort'><strong>".$gen."</strong></th>";
    echo "<th class='no-sort'><strong>".$gen."</strong></th>";
    $row2 .= "<th data-sort-method='number'>Count</th><th data-sort-method='number'>Ratio to ref.</th>";
    $ref_file = fopen($config["datapath2"]."/msg-".$gen."-main14-xsec-x86_64-slc6-gcc49-opt.dat" ,"r");
    if ($ref_file) {
      fgetcsv($ref_file, 0, "\t");
      while (($data = fgetcsv($ref_file, 0, "\t")) !== FALSE) {
        //$ref_data[$data[0]] = array($data[1] => array($gen => $data[2]));
        //
        $proc = $data[0];
        $msg = $data[1];
        $msg_count = $data[2];
        if (!array_key_exists($proc, $ref_data)) {
          $ref_data[$proc] = array();
        }

        if (!array_key_exists($msg, $ref_data[$proc])) {
          $ref_data[$proc][$msg] = array();
        }

        $ref_data[$proc][$msg][$gen] = $msg_count;
      }
      unset($proc);
      unset($msg);
      unset($msg_count);
      fclose($ref_file);
    }
    else {
      die("File not found: ".$config["datapath2"]."/msg-".$gen."-main14-xsec-x86_64-slc6-gcc49-opt.dat");
    }
  }
  echo "</tr>\n";
  //echo $row2."</tr>"
  /* $gen - $proc - $msg */
?>
            </thead>
            <tbody id="values">
<?php
  echo "<!--";
  print_r($ref_data);
  echo "-->";
  $first_row = TRUE;

  foreach($ref_data as $proc => $messages) {
    $first_row = TRUE;
    $is_nonzero = FALSE;
    foreach ($messages as $msg => $msg_count) {
      $row2 = $indent."<tr>";

      if ($first_row) {
        $row2 .= "<td>".preg_replace("/[+]([^-])/i", "+<br/>$1", $proc)."</td>";
        $first_row = FALSE;
      }
      else {
        $row2 .= "<td>&nbsp;</td>";
      }

      $row2 .= "<td>".str_replace("_", " ", $msg)."</td>";
      foreach ($refs as $gen) {
        $row2 .= "<td>".$ref_data[$proc][$msg][$gen]."</td>";
      }

      echo $row2."</tr>\n";
    }

      //$row2 .= "</tr>";
      //echo $row2;
      // $row2 .= "<td data-sort=".sprintf("%.15f", $ref_data[$gen][$proc])."><pre>".sprintf("%.3e", $ref_data[$gen][$proc])."</pre></td>";
      // if (($req_gen !== None) && (array_key_exists($req_gen, $ref_data))) {
      //   $ratio = $ref_data[$gen][$proc]/$ref_data[$req_gen][$proc];
      //   $ratiopc = $ratio*100;
      //   $tdstyle = "";
      //   if (($ratiopc > (100+$req_min)) || ($ratiopc < (100-$req_min))) {
      //     $tdstyle = " class=\"warn\"";
      //   }
      //   if (($ratiopc > (100+$req_max)) || ($ratiopc < (100-$req_max))) {
      //     $tdstyle = " class=\"err\"";
      //   }
      //   $row2 .= "<td".$tdstyle." data-sort=".sprintf("%.15f", $ratio)."><pre>".sprintf("%.3f", $ratio)."</pre></td>";
      // } else {
      //   $row2 .= "<td>N/A</td>";
      // }
    //}
    //echo $row2."</tr>\n";
  }
?>
            </tbody>
          </table>
        </div>
      </center>
    </p>
  </div>
</div>
