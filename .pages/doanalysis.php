<div id="content">
  <div class="post">
    <h1 class="title">Analysis results</h1>
    <div class="entry">
<p>
  Please wait while analysis becomes ready ...
</p>
<?php
$files = (isset($_REQUEST["files"]) ? $_REQUEST["files"] : array("none"));
$force = (isset($_REQUEST["force"]) ? $_REQUEST["force"] : "off");
if (in_array("none",$files)){
  echo "Exiting ...";
} else {
sort($files);
$filelist = join(" ", $files);
$html  = md5($filelist);
$rivetdir = $config["rivetdir"];
$htmldir = $config["htmldir"];
  if (! is_dir("$htmldir/$html") || $force == "on" ){
    $cmd = "python /afs/cern.ch/sw/lcg/releases/lcgenv/latest/lcgenv -p /afs/cern.ch/sw/lcg/releases/LCG_79 x86_64-slc6-gcc48-opt rivet 2.4.0 > /tmp/sourceme.sh; source /tmp/sourceme.sh; $rivetdir/bin/rivet-mkhtml --pdf -s --mc-errs -o \"$htmldir/$html\" ".join(" ",$files);
    $out = shell_exec("rm -rf \"$htmldir/$html\";mkdir -p \"$htmldir/$html\"; ($cmd; echo \$PYTHONPATH; echo \$PATH) |& tee $htmldir/$html/log.log");
#    echo "DEBUG : $out";
  };
  echo "Results have been prepared. See <a href=\"/".basename($htmldir)."/$html\">here</a>.";
}
?>
    </div>
  </div>
</div>

