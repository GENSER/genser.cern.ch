<div id="content">
  <div class="post">
    <h1 class="title">Filter</h1>
    <div class="entry">
<p>
Select filter conditions:
<form action="?" method="GET">
  <center>
  <table style="width:80%"><tr>
    <td><select name="generators[]" multiple size=6>
      <option selected value="all">All generators</option>
      <?php
        foreach($generators as $gen){
          echo "<option value=\"".urlencode($gen)."\">$gen</option>";
        }
      ?>
    </select></td>
    <td><select name="releases[]" multiple size=6>
      <option selected value="all">All releases</option>
      <?php
        foreach($releases as $rel){
          echo "<option value=\"".urlencode($rel)."\">$rel</option>";
        }
      ?>
    </select></td>
    <td><select name="platforms[]" multiple size=6>
      <option selected value="all">All platforms</option>
      <?php
        foreach($platforms as $pl){
          echo "<option value=\"".urlencode($pl)."\">$pl</option>";
        }
      ?>
    </select></td>
  </tr></table><br>
  <input type=hidden name=page value=filter>
  <input type=submit value="Continue ...">
  </center>
</form>
</p>
<p><center>
<hr>
<form action="../index.php" method="GET">
<input type=hidden name=page value="doanalysis">
<table style="width:100%">
<tr><td></td><td><strong>Generator</strong></td><td><strong>Version</strong></td><td><strong>Platform</strong></td><td><strong>Release</strong></td></tr>
<?php
$req_generators = (isset($_REQUEST["generators"]) ? $_REQUEST["generators"] : array("all"));
$req_platforms = (isset($_REQUEST["platforms"]) ? $_REQUEST["platforms"] : array("all"));
$req_releases = (isset($_REQUEST["releases"]) ? $_REQUEST["releases"] : array("all"));
foreach ($req_generators as &$val){
  $val = urldecode($val);
}

if (in_array("all",$req_generators)){
  $req_generators = $generators;
}
if (in_array("all",$req_releases)){
  $req_releases = $releases;
}
if (in_array("all",$req_platforms)){
  $req_platforms = $platforms;
}
foreach($DATA as $key => $tag){
  if (in_array($tag["platform"],$req_platforms) && in_array($tag["package"],$req_generators) && in_array("LCG_".$tag["release"],$req_releases)){
    echo "<tr><td> - </td><td><a href=\"?page=view&tag=".urlencode($key)."\">".$tag["package"]."</a></td><td>".$tag["version"]."</td><td>".$tag["platform"]."</td><td>LCG_".$tag["release"]."</td></tr>";
    $CONTENT = yaml_parse_file($config["datapath"] . "/$key/content.yaml");
    foreach($CONTENT["files"] as $f){
      if (preg_match("/.*\.yoda/",$f) && preg_match("/genser-(.*)-rivet/",$f,$matches)){
        echo '<tr><td></td><td><input name=files[] type=checkbox value="'.$config["datapath"].'/'.$key.'/'.urlencode($f).':\'Title:'.$key.'\'"></td><td colspan=3>'.strtoupper($matches[1]).'</td></tr>';
      }
    }
  }
}
?>
</table>
<center><input type=submit value="Make Rivet analysis"><input type=checkbox name=force>Force</center>
</form>
</center></p>
    </div>
  </div>
</div>

