<?php
$req_gen = (isset($_REQUEST["refs"]) ? $_REQUEST["refs"] : "None");
$req_min = (isset($_REQUEST["min"]) ? intval($_REQUEST["min"]) : 10);
$req_max = (isset($_REQUEST["max"]) ? intval($_REQUEST["max"]) : 30);

if ($req_min <= 0) $req_min = 10;
if ($req_max <= 0) $req_max = 30;
if ($req_min > $req_max) {
  $tmp = $req_max;
  $req_max = $req_min;
  $req_min = $tmp;
}

$indent = "                        ";
$gen_list = array();
$gen_list[] = "<option value disabled".(($req_gen == "None")?" selected":"")."> -- select version -- </option>\n";

foreach($refs as $gen) {
  $sel = ($gen == $req_gen)?" selected":"";
  $gen_list[]  = "<option value=\"".urlencode($gen)."\"$sel>$gen</option>\n";
}
?>

<div id="content" style="width: auto">
  <div class="post" style="background: none">
    <h1 class="title">Reference</h1>
    <div class="entry">
      <p>
<!--
        <form action="?" method="GET">
          <center>
            <div style="width: 100%">
              <table>
                <tr>
                  <td>Reference version:
                    <select name="refs">
<?php
foreach ($gen_list as $_ => $gen) {
  echo $indent.$gen;
}
?>
                    </select>
                  </td>
                  <td>Warning level: deviation &plusmn;
                    <input name="min" type="text" maxlength="2" value="<? echo $req_min ?>"> percent
                    ; Error level: deviation &plusmn;
                    <input name="max" type="text" maxlength="2" value="<? echo $req_max ?>"> percent
                  </td>
                  <td><input type=submit value="Use"></td>
                </tr>
              </table>
              <br>
              <input type=hidden name=page value=filter_ref>
            </center>
          </div>
        </form>
-->
      </p>
      <p>
        <center>
          <hr>
          <script type="text/javascript">
          window.onload = function() {
            var sidebar = document.getElementById('sidebar');
            sidebar.style.display = 'none';
            new Tablesort(document.getElementById('ref'));
          }
          </script>
          <script src='tablesort.min.js'></script>
          <script src='tablesort.numeric.js'></script>
          <table id="ref">
            <thead>
<?php
  $indent = "                  ";
  echo $indent."<tr style=\"border-bottom: none\"><th><strong>Process group</strong></th>";

  //$req_gen = (isset($_REQUEST["refs"]) ? $_REQUEST["refs"] : "None");
  $ref_data = array();
  $ref_proc = array();
  $row2 = $indent."<tr style=\"border-top: none; border-bottom: 1px solid black\"><th></th>";

  foreach($refs as $gen) {
//    echo "<th colspan=\"2\" class='no-sort'><strong>".$gen."</strong></th>";
    echo "<th class='no-sort'><strong>".$gen."</strong></th>";
    $row2 .= "<th data-sort-method='number'>Tries/Accepts</th><!-- th data-sort-method='number'>Ratio to ref.</th -->";
    $ref_file = fopen($config["datapath2"]."/tra-".$gen."-main14-xsec-x86_64-slc6-gcc49-opt.dat" ,"r");
    if ($ref_file) {
      fgetcsv($ref_file, 0, "\t");
      $ref_data[$gen] = array();
      while (($data = fgetcsv($ref_file, 0, "\t")) !== FALSE) {
        $proc = $data[0];
        $val = floatval($data[1]);
        $ref_data[$gen][$proc] = $val;
        $ref_proc[] = $proc;
      }
      fclose($ref_file);
    }
  }
  $ref_proc = array_unique($ref_proc);
  echo "</tr>\n";
  echo $row2."</tr>"
?>
            </thead>1
            <tbody id="values">
<?php
  foreach($ref_proc as $proc) {
    $row2 = $indent."<tr><td>".preg_replace("/[+]([^-])/i", "+<br/>$1", $proc)."</td>";
    foreach ($refs as $gen) {
      $val = $ref_data[$gen][$proc];
      $sval =  ($val < 0.0001) ? "N/A" : sprintf("%.4f", $ref_data[$gen][$proc]);
      $row2 .= "<td data-sort=".sprintf("%f", $ref_data[$gen][$proc])."><pre>$sval</pre></td>";
/*
      if (($req_gen !== None) && (array_key_exists($req_gen, $ref_data))) {
        $ratio = $ref_data[$gen][$proc]/$ref_data[$req_gen][$proc];
        $ratiopc = $ratio*100;
        $tdstyle = "";
        if (($ratiopc > (100+$req_min)) || ($ratiopc < (100-$req_min))) {
          $tdstyle = " class=\"warn\"";
        }
        if (($ratiopc > (100+$req_max)) || ($ratiopc < (100-$req_max))) {
          $tdstyle = " class=\"err\"";
        }
        $row2 .= "<td".$tdstyle." data-sort=".sprintf("%.15f", $ratio)."><pre>".sprintf("%.3f", $ratio)."</pre></td>";
      } else {
        $row2 .= "<td>N/A</td>";
      }
*/
    }
    echo $row2."</tr>\n";
  }
?>
            </tbody>
          </table>
        </div>
      </center>
    </p>
  </div>
</div>
