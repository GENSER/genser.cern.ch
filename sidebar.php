
        <div id="sidebar">
                <ul>
                        <li>
                                <h2>Releases</h2>
                                <ul>
<?php
  foreach($releases as $rel){
    echo "<li><a href=\"?page=filter&releases[]=".$rel."\">".$rel."</a></li>";
  }
?>
                                </ul>
                        </li>
                        <li>
                                <h2>Generators</h2>
                                <ul>
<?php
  foreach($generators as $gen){
    echo "<li><a href=\"?page=filter&generators[]=".urlencode(urlencode($gen))."\">".$gen."</a></li>";
  }
?>
                                </ul>
                        </li>
                        <li>
                                <h2>Pythia8 validation</h2>
                                <ul>
                                        <li><a href="?page=filter_ref">Cross Sections</a></li>
                                        <li><a href="?page=filter_msg">Warning/Error messages</a></li>
                                        <li><a href="?page=filter_tra">Tried/Accepted efficiency</a></li>
                                </ul>
                        </li> 
                </ul>
        </div>

