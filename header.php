<div id="header">
        <div id="logo">
                <h1><a href="/">GENSER Validation site</a></h1>
        </div>
        <div id="menu">
                <ul>
                        <li><a href="?page=filter">Results</a></li>
                        <li><a href="?page=contacts">About us</a></li>
                </ul>
        </div>
</div>


<?php
  $generators = array();
  $releases = array();
  $platforms = array();
  $refs = array();
  foreach($DATA as $tag){
    array_push($generators, $tag["package"]);
    array_push($releases, "LCG_".$tag["release"]);
    array_push($platforms, $tag["platform"]);
    if ($tag["package"] == "pythia8") {
      array_push($refs, $tag["package"]."-".$tag["version"]);
    }
  }
  $generators = array_unique($generators);
  $releases   = array_unique($releases);
  $platforms  = array_unique($platforms);
  sort($generators);
  sort($releases);
  sort($platforms);
?>
